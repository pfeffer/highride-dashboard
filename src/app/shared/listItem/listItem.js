angular.module('listItem', [])
.directive('listItem',['DeleteObjectService', '$state', function(DeleteObjectService, $state){
  return {
    restrict: 'EA',
    templateUrl: 'app/shared/listItem/listItem.html',
    link: function($scope) {
      $scope.selected = [];

      $scope.toggle = function (object, list) {
        var deleteBtn = angular.element( document.querySelector( '#delete-' + object.id ) );
        var idx = list.indexOf(object);

        if (idx > -1) {
          list.splice(idx, 1);
          deleteBtn.removeClass('block');
          deleteBtn.addClass('hidden');
        }
        else {
          list.push(object);
          deleteBtn.removeClass('hidden');
          deleteBtn.addClass('block');
        }
      };

      $scope.exists = function (object, list) {
        return list.indexOf(object) > -1;
      };

      $scope.delete = function() {
        DeleteObjectService.delete($scope.path + $scope.selected[0].id).then(function(response){
          console.log(response);
          $state.reload();
        }, function(response){
          console.log(response);
        });
      };

      // $scope.toggleAll = function() {
      //   if ($scope.selected.length === $scope.objects.length) {
      //     $scope.selected = [];
      //     var deleteBtn = angular.element( document.querySelector( '#delete-all' ) );

      //     deleteBtn.removeClass('block');
      //     deleteBtn.addClass('hidden');
      //   } else if ($scope.selected.length === 0 || $scope.selected.length > 0) {
      //     $scope.selected = $scope.objects.slice(0);
      //     var deleteBtn = angular.element( document.querySelector( '#delete-all' ) );

      //     deleteBtn.removeClass('hidden');
      //     deleteBtn.addClass('block');
      //   }
      // };
    }
  };
}]);