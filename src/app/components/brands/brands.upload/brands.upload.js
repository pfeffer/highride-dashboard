angular.module('brands.upload', [])
.controller('brandsUploadCtrl',['$scope', '$state', '$http', 'UploadImageService', 'UploadObjectService', function($scope, $state, $http, UploadImageService, UploadObjectService){
  $scope.name = "";
  $scope.description = "";

  $scope.background_rgb = "";
  $scope.colorOptions = {
    format: 'hex',
    restrictToFormat: true
  };

  $scope.dzOptions = {
    url : 'url',
    paramName : 'photo',
    maxFilesize : '10',
    acceptedFiles : 'image/jpeg, images/jpg, image/png',
    addRemoveLinks : true,
    maxFiles: 1,
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    }
  };

  $scope.dzCallbacks = {
    'addedfile' : function(file){
      // if ($scope.brandUploadForm.logo.$error.required){
      //   $scope.brandUploadForm.logo.$error = {};
      // }
      console.log(file);
      $scope.file = file;
    },
    'success' : function(file, xhr){
      console.log(file, xhr);
    }
  };

  $scope.dzMethods = {};
  $scope.removeNewFile = function(){
    $scope.dzMethods.removeFile($scope.file);
  };

  $scope.upload = function() {
    $scope.submitted = true;

    if ($scope.file === undefined) {
      $scope.brandUploadForm.logo = {};
      $scope.brandUploadForm.logo.$error = {};
      $scope.brandUploadForm.logo.$error.required = true;
    }

    if (Object.keys($scope.brandUploadForm.$error).length === 0){
      var logo = UploadImageService.uploadImage($scope.file);
      var data = {
        brand: {
          name: $scope.name,
          description: $scope.description,
          background_rgb: $scope.background_rgb,
          logo: logo
        }
      };

      UploadObjectService.upload('/brands', data).then(function(response){
        console.log(response.data);
        $state.go('brands.list');
      }, function(response){
        console.log(response);
      });
    }
  };
}]);