angular.module('brands.list', [])
.controller('brandsListCtrl',['$scope', '$state', '$http', 'GetObjectService', function($scope, $state, $http, GetObjectService){
  
  $scope.page = "Brands";
  $scope.path = "/brands/";
  $scope.order = "name";
  
  $scope.orderBy = function(order) {
    if (order === $scope.order) {
      $scope.order = '-' + order;
    }
    else {
      $scope.order = order;
    }
  };

  GetObjectService.getObjects($scope.path).then(function(objects){
    $scope.objects = objects;
  });

  $scope.viewObject = function(brandId) {
    $state.go('brands.view', {brandId: brandId});
  };
}]);