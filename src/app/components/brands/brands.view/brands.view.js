angular.module('brands.view', [])
.controller('brandsViewCtrl',['$scope', '$state', '$stateParams', '$http', 'UploadImageService', 'GetObjectService', 'EditObjectService', function($scope, $state, $stateParams, $http, UploadImageService, GetObjectService, EditObjectService){

  var brandId = $stateParams.brandId;

  GetObjectService.getObjects('/brands/' + brandId).then(function(object){
    $scope.object = object;
    console.log($scope.object);
    console.log($scope.object.background_rgb);
  });

  $scope.colorOptions = {
    format: 'hex8'
  };
  
  $scope.dzOptions = {
    url : 'url',
    paramName : 'photo',
    maxFilesize : '10',
    acceptedFiles : 'image/jpeg, images/jpg, image/png',
    addRemoveLinks : true,
    maxFiles: 1,
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    },
    init: function(){
      mockFile = { name: "myimage.jpg", size: 12345, type: 'image/jpeg', accepted: true };

      this.options.addedfile.call(this, mockFile);
      this.options.thumbnail.call(this, mockFile, $scope.object.logo);
      this.files.push(mockFile);

      mockFile.previewElement.classList.add('dz-success');
      mockFile.previewElement.classList.add('dz-complete');
    }
  };

  $scope.dzCallbacks = {
    'addedfile' : function(file){
      console.log(file);
      $scope.file = file;
    },
    'success' : function(file, xhr){
      console.log(file, xhr);
    }
  };

  $scope.dzMethods = {};
  $scope.removeNewFile = function(){
    $scope.dzMethods.removeFile($scope.file);
  };

  $scope.update = function(){
    var data = {
      brand: {
        name: $scope.object.name,
        description: $scope.object.description,
        background_rgb: $scope.object.background_rgb,
      }
    };

    if($scope.file) {
      var logo = UploadImageService.uploadImage($scope.file);
      data.brand.logo = logo;
    }

    EditObjectService.update('/brands/' + brandId, data).then(function(response){
      console.log(response.data);
      $state.go('brands.list');
    }, function(response){
      console.log(response);
    });
  };
}]);
