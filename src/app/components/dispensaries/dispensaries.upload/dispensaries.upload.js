angular.module('dispensaries.upload', [])
.controller('dispensariesUploadCtrl',['$scope', '$state', '$http', 'UploadImageService', 'UploadObjectService', function($scope, $state, $http, UploadImageService, UploadObjectService){
  $scope.name = "";
  $scope.description = "";
  $scope.address = "";
  $scope.city = "";
  $scope.state = "";
  $scope.postal_code = "";
  
  $scope.dzOptions = {
    url : 'url',
    paramName : 'photo',
    maxFilesize : '10',
    acceptedFiles : 'image/jpeg, images/jpg, image/png',
    addRemoveLinks : true,
    maxFiles: 1,
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    }
  };

  $scope.dzCallbacks = {
    'addedfile' : function(file){
      // if ($scope.dispensariesUploadForm.logo.$error.required){
      //   $scope.dispensariesUploadForm.logo.$error = {};
      // }
      console.log(file);
      $scope.file = file;
    },
    'success' : function(file, xhr){
      console.log(file, xhr);
    }
  };

  $scope.dzMethods = {};
  $scope.removeNewFile = function(){
    $scope.dzMethods.removeFile($scope.file);
  };

  $scope.upload = function() {
    $scope.submitted = true;

    if ($scope.file === undefined) {
      $scope.dispensariesUploadForm.logo = {};
      $scope.dispensariesUploadForm.logo.$error = {};
      $scope.dispensariesUploadForm.logo.$error.required = true;
    }

    if (Object.keys($scope.dispensariesUploadForm.$error).length === 0){
      var logo = UploadImageService.uploadImage($scope.file);
      var url = ('https://maps.googleapis.com/maps/api/geocode/json?address=' + $scope.address + '+' + $scope.city + ',+' + $scope.state + '&key=AIzaSyCRjqkLrgtEpj3ACBNdB6-uZM2Ypvu4g5c').replace(' ', '+');

      $http({
        method: 'GET',
        url: url,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
      }).then(function(response){
        $scope.location = {lat: response.data.results[0].geometry.location.lat, long: response.data.results[0].geometry.location.lng};
        var data = {
          store: {
            name: $scope.name,
            description: $scope.description,
            address: $scope.address,
            location: $scope.location,
            city: $scope.city,
            state: $scope.state,
            postal_code: $scope.postal_code,
            logo: logo
          }
        };
        
        UploadObjectService.upload('/stores', data).then(function(response){
          console.log(response.data);
          $state.go('dispensaries.list');
        }, function(response){
          console.log(response);
        });

      }, function(response){
        console.log(response);
      });
    }
  };
}]);
