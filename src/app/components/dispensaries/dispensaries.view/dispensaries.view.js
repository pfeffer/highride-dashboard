angular.module('dispensaries.view', [])
.controller('dispensariesViewCtrl',['$scope', '$state', '$stateParams', '$http', 'UploadImageService', 'GetObjectService', 'EditObjectService', function($scope, $state, $stateParams, $http, UploadImageService, GetObjectService, EditObjectService){

  var dispensaryId = $stateParams.dispensaryId;

  GetObjectService.getObjects('/stores/' + dispensaryId).then(function(object){
    $scope.object = object;
  });

  $scope.dzOptions = {
    url : 'url',
    paramName : 'photo',
    maxFilesize : '10',
    acceptedFiles : 'image/jpeg, images/jpg, image/png',
    addRemoveLinks : true,
    maxFiles: 1,
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    },
    init: function(){
      mockFile = { name: "myimage.jpg", size: 12345, type: 'image/jpeg', accepted: true };

      this.options.addedfile.call(this, mockFile);
      this.options.thumbnail.call(this, mockFile, $scope.object.logo);
      this.files.push(mockFile);

      mockFile.previewElement.classList.add('dz-success');
      mockFile.previewElement.classList.add('dz-complete');
    }
  };
  
  $scope.dzCallbacks = {
    'addedfile' : function(file){
      console.log(file);
      $scope.file = file;
    },
    'success' : function(file, xhr){
      console.log(file, xhr);
    }
  };

  $scope.dzMethods = {};
  $scope.removeNewFile = function(){
    $scope.dzMethods.removeFile($scope.file);
  };

  $scope.update = function(){
    $scope.submitted = true;

    if (Object.keys($scope.dispensariesUploadForm.$error).length === 0){
      var data = {
        store: {
          name: $scope.object.name,
          description: $scope.object.description,
          address: $scope.object.address,
          location: $scope.object.location,
          city: $scope.object.city,
          state: $scope.object.state,
          postal_code: $scope.object.postal_code,
        }
      };

      if($scope.file) {
        var logo = UploadImageService.uploadImage($scope.file);
        data.store.logo = logo;
      }

      EditObjectService.update('/stores/' + dispensaryId, data).then(function(response){
        console.log(response.data);
        $state.go('dispensaries.list');
      }, function(response){
        console.log(response);
      });
    }
  };
}]);