angular.module('dispensaries.list', [])
.controller('dispensariesListCtrl',['$scope', '$state', '$http', 'GetObjectService', function($scope, $state, $http, GetObjectService){
  $scope.page = "Dispensaries";
  $scope.path = "/stores/";
  $scope.order = "name";
  
  $scope.orderBy = function(order) {
    if (order === $scope.order) {
      $scope.order = '-' + order;
    }
    else {
      $scope.order = order;
    }
  };
  
  GetObjectService.getObjects($scope.path).then(function(objects){
    $scope.objects = objects;
  });
  
  $scope.viewObject = function(dispensaryId) {
    $state.go('dispensaries.view', {dispensaryId: dispensaryId});
  };
}]);