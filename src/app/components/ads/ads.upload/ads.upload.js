angular.module('ads.upload', [])
.controller('adsUploadCtrl',['$scope', '$state', '$http', 'UploadImageService', 'UploadObjectService', function($scope, $state, $http, UploadImageService, UploadObjectService){
  $scope.title = "";
  $scope.adType = "";
  $scope.enabled = "";
  $scope.website = "";

  $scope.dzOptions = {
    url : 'url',
    paramName : 'photo',
    maxFilesize : '10',
    acceptedFiles : 'image/jpeg, images/jpg, image/png',
    addRemoveLinks : true,
    maxFiles: 1,
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    }
  };

  $scope.dzCallbacks = {
    'addedfile' : function(file){
      // if ($scope.adsUploadForm.image.$error.required){
      //   $scope.adsUploadForm.image.$error = {};
      // }
      console.log(file);
      $scope.file = file;
    },
    'success' : function(file, xhr){
      console.log(file, xhr);
    }
  };

  $scope.dzMethods = {};
  $scope.removeNewFile = function(){
    $scope.dzMethods.removeFile($scope.file);
  };

  $scope.upload = function() {
    $scope.submitted = true;

    if ($scope.file === undefined) {
      $scope.adsUploadForm.image = {};
      $scope.adsUploadForm.image.$error = {};
      $scope.adsUploadForm.image.$error.required = true;
    }

    if (Object.keys($scope.adsUploadForm.$error).length === 0){
      var image = UploadImageService.uploadImage($scope.file);
      var data = {
        ads: {
          title: $scope.title,
          ad_type: $scope.adType,
          enabled: $scope.enabled,
          website: $scope.website,
          image: image
        }
      };

      UploadObjectService.upload('ads', data).then(function(response){
        console.log(response.data);
        $state.go('ads.list');
      }, function(response){
        console.log(response);
      });
    }
  };
}]);