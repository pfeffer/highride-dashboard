angular.module('ads.list', [])
.controller('adsListCtrl',['$scope', '$state', '$http', 'GetObjectService', function($scope, $state, $http, GetObjectService){
  $scope.page = "Ads";
  $scope.path = "/ads/";
  $scope.order = "name";
  
  $scope.orderBy = function(order) {
    if (order === $scope.order) {
      $scope.order = '-' + order;
    }
    else {
      $scope.order = order;
    }
  };

  GetObjectService.getObjects($scope.path).then(function(objects){
    $scope.objects = objects;
  });
  
  $scope.viewObject = function(adId) {
    $state.go('ads.view', {adId: adId});
  };
}]);