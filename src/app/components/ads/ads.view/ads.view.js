angular.module('ads.view', [])
.controller('adsViewCtrl',['$scope', '$state', '$stateParams', '$http', 'UploadImageService', 'GetObjectService', 'EditObjectService', function($scope, $state, $stateParams, $http, UploadImageService, GetObjectService, EditObjectService){

  var adId = $stateParams.adId;

  GetObjectService.getObjects('/ads/' + adId).then(function(object){
    $scope.object = object;
  });

  $scope.dzOptions = {
    url : 'url',
    paramName : 'photo',
    maxFilesize : '10',
    acceptedFiles : 'image/jpeg, images/jpg, image/png',
    addRemoveLinks : true,
    maxFiles: 1,
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    },
    init: function(){
      mockFile = { name: "myimage.jpg", size: 12345, type: 'image/jpeg', accepted: true };

      this.options.addedfile.call(this, mockFile);
      this.options.thumbnail.call(this, mockFile, $scope.object.image);
      this.files.push(mockFile);

      mockFile.previewElement.classList.add('dz-success');
      mockFile.previewElement.classList.add('dz-complete');
    }
  };

  $scope.dzCallbacks = {
    'addedfile' : function(file){
      console.log(file);
      $scope.file = file;
    },
    'success' : function(file, xhr){
      console.log(file, xhr);
    }
  };

  $scope.dzMethods = {};
  $scope.removeNewFile = function(){
    $scope.dzMethods.removeFile($scope.file);
  };

  $scope.update = function(){
    var data = {
      ad: {
        title: $scope.object.title,
        ad_type: $scope.object.adType,
        enabled: $scope.object.enabled,
        website: $scope.object.website,
      }
    };

    if($scope.file) {
      var image = UploadImageService.uploadImage($scope.file);
      data.ad.image = image;
    }

    EditObjectService.update('/ads/' + adId, data).then(function(response){
      console.log(response.data);
      $state.go('ads.list');
    }, function(response){
      console.log(response);
    });
  };
}]);
