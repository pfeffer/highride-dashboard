angular.module('products.view', [])
.controller('productsViewCtrl',['$scope', '$state', '$stateParams', '$http', 'UploadImageService', 'GetObjectService', 'EditObjectService', function($scope, $state, $stateParams, $http, UploadImageService, GetObjectService, EditObjectService){
  var productId = $stateParams.productId;

  GetObjectService.getObjects('/brands').then(function(brands){
    $scope.brands = brands;
  });
  
  GetObjectService.getObjects('/products/' + productId).then(function(object){
    $scope.object = object;
  });

  $scope.dzOptions = {
    url : 'url',
    paramName : 'photo',
    maxFilesize : '10',
    acceptedFiles : 'image/jpeg, images/jpg, image/png',
    addRemoveLinks : true,
    maxFiles: 1,
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    },
    init: function(){
      mockFile = { name: "myimage.jpg", size: 12345, type: 'image/jpeg', accepted: true };

      this.options.addedfile.call(this, mockFile);
      this.options.thumbnail.call(this, mockFile, $scope.object.featured_image);
      this.files.push(mockFile);

      mockFile.previewElement.classList.add('dz-success');
      mockFile.previewElement.classList.add('dz-complete');
    }
  };
  
  $scope.dzCallbacks = {
    'addedfile' : function(file){
      console.log(file);
      $scope.file = file;
    },
    'success' : function(file, xhr){
      console.log(file, xhr);
    }
  };

  $scope.dzMethods = {};
  $scope.removeNewFile = function(){
    $scope.dzMethods.removeFile($scope.file);
  };

  $scope.update = function(){
    var data = {
      product: {
        name: $scope.object.name,
        brand: $scope.object.brand.id
      }
    };

    if($scope.file) {
      var featured_image = UploadImageService.uploadImage($scope.file);
      data.product.featured_image = featured_image;
    }

    EditObjectService.update('/products/' + productId, data).then(function(response){
      console.log(response.data);
      $state.go('products.list');
    }, function(response){
      console.log(response);
    });
  };
}]);