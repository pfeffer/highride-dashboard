angular.module('products.upload', [])
.controller('productsUploadCtrl',['$scope', '$state', '$http', 'GetObjectService', 'UploadImageService', 'UploadObjectService', function($scope, $state, $http, GetObjectService, UploadImageService, UploadObjectService){

  $scope.name = "";
  $scope.brand = "";

  GetObjectService.getObjects('/brands').then(function(brands){
    $scope.brands = brands;
  });

  $scope.dzOptions = {
    url : 'url',
    paramName : 'photo',
    maxFilesize : '10',
    acceptedFiles : 'image/jpeg, images/jpg, image/png',
    addRemoveLinks : true,
    maxFiles: 1,
    maxfilesexceeded: function(file) {
      this.removeAllFiles();
      this.addFile(file);
    }
  };

  $scope.dzCallbacks = {
    'addedfile' : function(file){
      // if ($scope.productsUploadForm.featured_image.$error.required){
      //   $scope.productsUploadForm.featured_image.$error = {};
      // }
      console.log(file);
      $scope.file = file;
    },
    'success' : function(file, xhr){
      console.log(file, xhr);
    }
  };

  $scope.dzMethods = {};
  $scope.removeNewFile = function(){
    $scope.dzMethods.removeFile($scope.file);
  };

  $scope.upload = function(){
    $scope.submitted = true;

    if ($scope.file === undefined) {
      $scope.productsUploadForm.featured_image = {};
      $scope.productsUploadForm.featured_image.$error = {};
      $scope.productsUploadForm.featured_image.$error.required = true;
    }

    if (Object.keys($scope.productsUploadForm.$error).length === 0){
      var featured_image = UploadImageService.uploadImage($scope.file);
      var data = {
        product: {
          name: $scope.name,
          brand: $scope.brand,
          featured_image: featured_image
        }
      };
      
      UploadObjectService.upload('/products', data).then(function(response){
        console.log(response.data);
        $state.go('products.list');
      }, function(response){
        console.log(response);
      });
    }
  };
}]);