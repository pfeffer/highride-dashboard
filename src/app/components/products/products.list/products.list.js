angular.module('products.list', [])
.controller('productsListCtrl',['$scope', '$state', '$http', 'GetObjectService', function($scope, $state, $http, GetObjectService){

  $scope.page = "Products";
  $scope.path = "/products/";
  $scope.order = "name";

  $scope.orderBy = function(order) {
    if (order === $scope.order) {
      $scope.order = '-' + order;
    }
    else {
      $scope.order = order;
    }
  };

  GetObjectService.getObjects($scope.path).then(function(objects){
    $scope.objects = objects;
  });
  
  $scope.viewObject = function(productId) {
    $state.go('products.view', {productId: productId});
  };
}]);
