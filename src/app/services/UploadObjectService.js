angular.module('UploadObjectService', [])
.service('UploadObjectService', ['$http', function($http){
  this.upload = function(path, data){ 
    return $http({
      method: 'POST',
      url: 'https://api.usehighride.com/v1' + path,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      data: data
    });
  };
}]);
