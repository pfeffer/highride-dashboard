angular.module('GetObjectService', [])
.service('GetObjectService', ['$http', function($http){
  this.getObjects = function(path){ 
    return $http({
      method: 'GET',
      url: 'https://api.usehighride.com/v1' + path,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
    }).then(function(response){
      console.log(response.data);
      return response.data;
    }, function(response){
      return response;
    });
  };
}]);
