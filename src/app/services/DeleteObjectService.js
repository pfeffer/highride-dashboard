angular.module('DeleteObjectService', [])
.service('DeleteObjectService', ['$http', function($http){
  this.delete = function(path){
    return $http({
      method: 'DELETE',
      url: 'https://api.usehighride.com/v1' + path
    });
  };
}]);
