angular.module('UploadImageService', [])
.service('UploadImageService', [function(){
  
  var sizeLimit      = 10585760; // 10MB in Bytes
  var uploadProgress = 0;
  var creds = {
    bucket: 'highride',
    access_key: ACCESS_KEY,
    secret_key: SECRET_KEY
  };

  var fileSizeLabel = function() {
  // Convert Bytes To MB
    return Math.round(sizeLimit / 1024 / 1024) + 'MB';
  };

  var uniqueString = function() {
    var text     = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 8; i++ ) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  };

  this.uploadImage = function(file) {
    AWS.config.update({ accessKeyId: creds.access_key, secretAccessKey: creds.secret_key });
    AWS.config.region = 'us-east-1';
    var bucket = new AWS.S3({ params: { Bucket: creds.bucket } });
    
    // Perform File Size Check First
    var fileSize = Math.round(parseInt(file.size));
    if (fileSize > sizeLimit) {
      console.log('Sorry, your attachment is too big. <br/> Maximum '  + fileSizeLabel() + ' file attachment allowed','File Too Large');
      return false;
    }
    // Prepend Unique String To Prevent Overwrites
    var uniqueFileName = uniqueString() + '-' + file.name;
    var url = "https://s3-us-west-2.amazonaws.com/highride/" + uniqueFileName;
    console.log(url);
    var params = { Key: uniqueFileName, ContentType: file.type, Body: file, ServerSideEncryption: 'AES256' };

    bucket.putObject(params, function(err, data) {
      if(err) {
        console.log(err.message,err.code);
        return false;
      }
      else {
        // Upload Successfully Finished
        console.log('File Uploaded Successfully', 'Done');
      }
    });
    return url;
  };
}]);
