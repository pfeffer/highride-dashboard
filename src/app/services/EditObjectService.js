angular.module('EditObjectService', [])
.service('EditObjectService', ['$http', function($http){
  this.update = function(path, data){ 
    return $http({
      method: 'PATCH',
      url: 'https://api.usehighride.com/v1' + path,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      data: data
    });
  };
}]);
