require('angular');
require('angular-ui-router');
require('angular-aria');
require('angular-animate');
require('angular-material');
require('angular-messages');
require('ngdropzone');
require('angularjs-color-picker');
require('./services/UploadImageService.js');
require('./services/GetObjectService.js');
require('./services/UploadObjectService.js');
require('./services/EditObjectService.js');
require('./services/DeleteObjectService.js');
require('./components/login/login.js');
require('./components/products/products.list/products.list.js');
require('./components/products/products.upload/products.upload.js');
require('./components/products/products.view/products.view.js');
require('./components/dispensaries/dispensaries.list/dispensaries.list.js');
require('./components/dispensaries/dispensaries.upload/dispensaries.upload.js');
require('./components/dispensaries/dispensaries.view/dispensaries.view.js');
require('./components/brands/brands.list/brands.list.js');
require('./components/brands/brands.upload/brands.upload.js');
require('./components/brands/brands.view/brands.view.js');
require('./components/ads/ads.list/ads.list.js');
require('./components/ads/ads.upload/ads.upload.js');
require('./components/ads/ads.view/ads.view.js');
require('./components/offers/offers.list/offers.list.js');
require('./shared/header/header.js');
require('./shared/listItem/listItem.js');

var app = angular.module('app', [
	'ui.router',
	'ngMaterial',
	'ngMessages',
	'thatisuday.dropzone',
	'color.picker',
	'DeleteObjectService',
	'UploadImageService',
	'GetObjectService',
	'UploadObjectService',
	'EditObjectService',
	'header',
	'login',
	'listItem',
	'products.list', 
	'products.upload', 
	'products.view', 
	'dispensaries.list', 
	'dispensaries.upload', 
	'dispensaries.view', 
	'brands.list', 
	'brands.upload', 
	'brands.view',
	'ads.list', 
	'ads.upload', 
	'ads.view',
	'offers.list']);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider) {
	
	$stateProvider
	.state('products', {
		url: "/products",
		abstract: true,
    template: "<div ui-view></div>"
	})
	.state('products.list', {
		url: "",
		views : {
			"" : {
				templateUrl:"app/components/products/products.list/products.list.html"
			},
			"header@products.list":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@products.list":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"list@products.list":{
				templateUrl:"app/shared/list/list.html"
			}
		}
	})
	.state('products.upload', {
		url: "/upload",
		views : {
			"" : {
				templateUrl:"app/components/products/products.upload/products.upload.html"
			},
			"header@products.upload":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@products.upload":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"list@products.upload":{
				templateUrl:"app/shared/list/list.html"
			},
			"dropzone@products.upload":{
				templateUrl:"app/shared/dropzone/dropzone.html"
			}
		}
	})
	.state('products.view', {
		url: "/:productId",
		views : {
			"" : {
				templateUrl:"app/components/products/products.view/products.view.html"
			},
			"header@products.view":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@products.view":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"dropzone@products.view":{
				templateUrl:"app/shared/dropzone/dropzone.html"
			}
		}
	})
	.state('dispensaries', {
		url: "/dispensaries",
		abstract: true,
    template: "<div ui-view></div>"
	})
	.state('dispensaries.list', {
		url: "",
		views : {
			"" : {
				templateUrl:"app/components/dispensaries/dispensaries.list/dispensaries.list.html"
			},
			"header@dispensaries.list":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@dispensaries.list":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"list@dispensaries.list":{
				templateUrl:"app/shared/list/list.html"
			}
		}
	})
	.state('dispensaries.upload', {
		url: "/upload",
		views : {
			"" : {
				templateUrl:"app/components/dispensaries/dispensaries.upload/dispensaries.upload.html"
			},
			"header@dispensaries.upload":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@dispensaries.upload":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"list@dispensaries.upload":{
				templateUrl:"app/shared/list/list.html"
			},
			"dropzone@dispensaries.upload":{
				templateUrl:"app/shared/dropzone/dropzone.html"
			}
		}
	})
	.state('dispensaries.view', {
		url: "/:dispensaryId",
		views : {
			"" : {
				templateUrl:"app/components/dispensaries/dispensaries.view/dispensaries.view.html"
			},
			"header@dispensaries.view":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@dispensaries.view":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"dropzone@dispensaries.view":{
				templateUrl:"app/shared/dropzone/dropzone.html"
			}
		}
	})
	.state('brands', {
		url: "/brands",
		abstract: true,
    template: "<div ui-view></div>"
	})
	.state('brands.list', {
		url: "",
		views : {
			"" : {
				templateUrl:"app/components/brands/brands.list/brands.list.html"
			},
			"header@brands.list":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@brands.list":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"list@brands.list":{
				templateUrl:"app/shared/list/list.html"
			}
		}
	})
	.state('brands.upload', {
		url: "/upload",
		views : {
			"" : {
				templateUrl:"app/components/brands/brands.upload/brands.upload.html"
			},
			"header@brands.upload":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@brands.upload":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"list@brands.upload":{
				templateUrl:"app/shared/list/list.html"
			},
			"dropzone@brands.upload":{
				templateUrl:"app/shared/dropzone/dropzone.html"
			},
		}
	})
	.state('brands.view', {
		url: "/:brandId",
		views : {
			"" : {
				templateUrl:"app/components/brands/brands.view/brands.view.html"
			},
			"header@brands.view":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@brands.view":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"dropzone@brands.view":{
				templateUrl:"app/shared/dropzone/dropzone.html"
			}
		}
	})
	.state('ads', {
		url: "/ads",
		abstract: true,
    template: "<div ui-view></div>"
	})
	.state('ads.list', {
		url: "",
		views : {
			"" : {
				templateUrl:"app/components/ads/ads.list/ads.list.html"
			},
			"header@ads.list":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@ads.list":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"list@ads.list":{
				templateUrl:"app/shared/list/list.html"
			}
		}
	})
	.state('ads.upload', {
		url: "/upload",
		views : {
			"" : {
				templateUrl:"app/components/ads/ads.upload/ads.upload.html"
			},
			"header@ads.upload":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@ads.upload":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"list@ads.upload":{
				templateUrl:"app/shared/list/list.html"
			},
			"dropzone@ads.upload":{
				templateUrl:"app/shared/dropzone/dropzone.html"
			},
		}
	})
	.state('ads.view', {
		url: "/:adId",
		views : {
			"" : {
				templateUrl:"app/components/ads/ads.view/ads.view.html"
			},
			"header@ads.view":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@ads.view":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"dropzone@ads.view":{
				templateUrl:"app/shared/dropzone/dropzone.html"
			}
		}
	})
	.state('offers', {
		url: "/offers",
		abstract: true,
    template: "<div ui-view></div>"
	})
	.state('offers.list', {
		url: "",
		views : {
			"" : {
				templateUrl:"app/components/offers/offers.list/offers.list.html"
			},
			"header@offers.list":{
				templateUrl:"app/shared/header/header.html"
			},
			"sidebar@offers.list":{
				templateUrl:"app/shared/sidebar/sidebar.html"
			},
			"list@offers.list":{
				templateUrl:"app/shared/list/list.html"
			}
		}
	});

  $locationProvider.html5Mode(true);
});
